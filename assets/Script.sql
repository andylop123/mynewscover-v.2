CREATE DATABASE mynewscover;
CREATE TABLE rol(
  id int AUTO_INCREMENT PRIMARY key,
  nombre text NOT null
);
INSERT INTO
  rol(nombre)
VALUES
  ('Usuario');
INSERT INTO
  rol(nombre)
VALUES
  ('Administrador');
CREATE TABLE users(
    id int AUTO_INCREMENT PRIMARY key,
    name text NOT null,
    lastname text NOT null,
    email text NOT null,
    password text NOT null,
    id_rol int not null,
    verify boolean DEFAULT False,
    FOREIGN KEY(id_rol) REFERENCES rol(id)
  );

  CREATE table category(
id int AUTO_INCREMENT PRIMARY KEY,
    nombre text not null
);


CREATE TABLE source(
    id int AUTO_INCREMENT PRIMARY key,
    url text NOT null,
    name text NOT null,
    id_user int not null,
    id_category int not null,
    private boolean default true,
    FOREIGN KEY(id_user) REFERENCES users(id),
    FOREIGN KEY(id_category) REFERENCES category(id)
  );
  

CREATE TABLE tags(
    id int AUTO_INCREMENT PRIMARY key,
    name text NOT null,
    id_user int not null,
    id_category int not null,
    FOREIGN KEY(id_user) REFERENCES users(id),
    FOREIGN KEY(id_category) REFERENCES category(id)
  );


CREATE TABLE news(
    id int AUTO_INCREMENT PRIMARY key,
    title text NOT null,
    short_description text NOT null,
    permanlink text NOT null,
    fecha date NOT null,
    id_source int not null,
    id_user int not null,
    id_category int not null,
    name_tag text not null,
    FOREIGN KEY(id_source) REFERENCES source(id),
    FOREIGN KEY(id_user) REFERENCES users(id),
    FOREIGN KEY(id_category) REFERENCES category(id)
  );