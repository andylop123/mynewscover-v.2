<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
		integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>New Sources</title>
</head>

<body>
	<div class="container">
		<header class="bg-white ">
			<nav class="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center ">
				<img src="<?php echo base_url('assets/img/noticias.svg'); ?>" width="230" height="80"
					class="d-inline-block align-top" alt="" loading="lazy">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
					aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" id="button-nav">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
					<ul class="navbar-nav text-secondary">


						<li class="nav-item">
							<a class="nav-link bg-secondary text-white" href=" <?php echo site_url('user/login') ?>"
								tabindex="-1" aria-disabled="true">
								<img src="<?php echo base_url('assets/img/icons8_exit_32px.png'); ?>" width="20"
									height="20" class="d-inline-block align-top" alt="" loading="lazy"> Exit</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
	</div>

	<!-- Header -->

	<div class="container pt-1">
		<div class="jumbotron bg-white text-secondary ">
			<h4 class="display-6 text-center">Public Covers</h4>
			<hr class="my-4 bg-secondary w-25">

		</div>
	</div>




	<!-- tittle -->
	<div class="container ">
		<main class="bg-white  d-flex flex-column align-items-center pt-4 pt-0 " style="margin-top: -7rem; ">
			<table class="table  table-dark table-striped  w-50">
				<thead>
					<tr>
						<th scope="col">Name</th>
						<th scope="col">Link</th>

					</tr>
				</thead>
				<tbody>
					<?php
                    foreach ($links as $link) { ?>
					<tr>
						<td><?php echo $link->name . ' ' . $link->lastname; ?></td>
						<td><a
								href="<?php echo site_url(['PublicCover', $link->name, $link->lastname]) ?>"><?php echo site_url(['PublicCover', $link->name, $link->lastname]) ?></a>
						</td>



					</tr>
					<?php } ?>
				</tbody>
			</table>
			<div>
			</div>
		</main>
	</div>


	<div class="container pt-5 ">
		<footer class="bg-white  pt-2">
			<ul class="nav justify-content-center ">
				<li class="nav-item active ">
					<a class="nav-link text-secondary " href="">My Cover </a>
				</li>
				<li class="nav-item ">
					<a class="nav-link text-secondary ">|</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link text-secondary " href="">About</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link text-secondary ">|</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link text-secondary " href="# ">Help</a>
				</li>
			</ul>
			<ul class="nav justify-content-center ">
				<a class="nav-link text-secondary " href="# " tabindex="-1 " aria-disabled="true ">© My News Cover
				</a>
			</ul>
		</footer>
	</div>
</body>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
</script>
