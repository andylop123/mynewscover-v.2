<?php

$user = $this->session->userdata('user');
?>


<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
		integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="style.css">

	<title>Add Category</title>
</head>

<body>
	<div class="container">
		<header class="bg-white ">
			<nav class="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center ">
				<img src="<?php echo base_url('assets/img/noticias.svg'); ?>" width="230" height="80"
					class="d-inline-block align-top" alt="" loading="lazy">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
					aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" id="button-nav">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
					<ul class="navbar-nav text-secondary">
						<li class="nav-item dropdown bg-secondary">
							<a class="nav-link dropdown-toggle text-white" data-bs-toggle="dropdown" href="#"
								role="button" aria-expanded="false"><img
									src="<?php echo base_url('assets/img/icons8_user_32px_2.png'); ?>" width="20"
									height="20" class="d-inline-block align-top" alt="" loading="lazy">
								<?php echo $user->name; ?></a>
							<ul class="dropdown-menu">
								<li> <a class="nav-link text-secondary" href="<?php echo site_url('category/index') ?>"
										tabindex="-1" aria-disabled="true">Categories</a></li>
								<li> <a class="nav-link bg-secondary text-white"
										href="<?php echo site_url('user/logout') ?>" tabindex="-1"
										aria-disabled="true">Logout <img
											src="<?php echo base_url('assets/img/icons8_user_32px_2.png'); ?>"
											width="20" height="20" class="d-inline-block align-top" alt=""
											loading="lazy"></a></li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
	</div>
	<!-- header -->
	<div class="container pt-1">
		<div class="jumbotron bg-white text-secondary ">
			<h4 class="display-6 text-center">Categories</h4>
			<hr class="my-4 bg-secondary w-25">

		</div>
	</div>

	<!-- tittle -->

	<div class="container ">
		<main class="bg-white pt-4 pt-0" style="margin-top: -2rem;">
			<form class="text-center" id="form" method="POST" action=<?php echo site_url('category/addCategory') ?>>
				<div class="form-row justify-content-center">
				</div>

				<div class="form-row d-flex flex-column align-items-center">

					<div class="form-group col-md-3 ">
						<!-- <label for="inputAddress">Address</label> -->
						<?php if ($message = $this->session->flashdata('msg')) { ?>
						<p class="alert alert-success agileits text-center w-50 " role="alert"> <?php echo $message; ?>
						</p>
						<?php }  ?>
					</div>
					<div class="form-group col-md-3">
						<input type="text" class="form-control" placeholder="Category"
							aria-describedby="inputGroupPrepend" required name="nombre">
					</div>


					<hr class=" bg-secondary w-50">
					<button type="submit" class="btn btn-secondary btn-sm m-1 mb-4" id="button-Register">Save</button>
				</div>
			</form>
		</main>
	</div>

	<!-- footer -->
	<div class="container pt-5 mt-3">
		<footer class="bg-white  pt-5 mt-5">
			<ul class="nav justify-content-center ">
				<li class="nav-item active ">
					<a class="nav-link text-secondary " href="">My Cover </a>
				</li>
				<li class="nav-item ">
					<a class="nav-link text-secondary ">|</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link text-secondary " href="">About</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link text-secondary ">|</a>
				</li>
				<li class="nav-item ">
					<a class="nav-link text-secondary " href="# ">Help</a>
				</li>
			</ul>
			<ul class="nav justify-content-center ">
				<a class="nav-link text-secondary " href="# " tabindex="-1 " aria-disabled="true ">© My News Cover
				</a>
			</ul>
		</footer>
	</div>
</body>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
</script>

</html>
