<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
		integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>News Cover</title>
</head>

<div class="container">
	<header class="bg-white ">
		<nav class="navbar navbar-expand-lg navbar-light bg-white text-secondary align-items-center ">
			<img src="<?php echo base_url('assets/img/noticias.svg'); ?>" width="230" height="80"
				class="d-inline-block align-top" alt="" loading="lazy">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
				aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation" id="button-nav">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse justify-content-end" id="navbarNav">
				<ul class="navbar-nav text-secondary">
					<li class="nav-item dropdown bg-secondary">
						<a class="nav-link dropdown-toggle text-white" data-bs-toggle="dropdown" href="#" role="button"
							aria-expanded="false"><img
								src="<?php echo base_url('assets/img/icons8_user_32px_2.png'); ?>" width="20"
								height="20" class="d-inline-block align-top" alt="" loading="lazy">
							<?php echo $user->name ?></a>
						<ul class="dropdown-menu">
							<li><a class="nav-link text-secondary" href="<?php echo site_url('news/publiccover') ?>"
									tabindex="-1" aria-disabled="true">Post my cover</a></li>
							<li> <a class="nav-link text-secondary" href="<?php echo site_url('source/showSource') ?>"
									tabindex="-1" aria-disabled="true">New Sources</a></li>
							<li> <a class="nav-link bg-secondary text-white"
									href="<?php echo site_url('user/logout') ?>" tabindex="-1"
									aria-disabled="true">Logout <img
										src="<?php echo base_url('assets/img/icons8_user_32px_2.png'); ?>" width="20"
										height="20" class="d-inline-block align-top" alt="" loading="lazy"></a></li>
						</ul>
					</li>
				</ul>
			</div>
		</nav>
	</header>
</div>

<!-- Header -->

<div class="container pt-1">
	<div class="jumbotron bg-white text-secondary ">
		<h4 class="display-6 text-center">Your Unique News Cover</h4>
		<hr class="my-4 bg-secondary w-25">

	</div>
</div>

<!-- tittle -->
<div class="container  mb-5 pb-5 d-flex flex-column align-items-center w-75">
	<div class="card-group ">
		<a class="btn btn-secondary  m-1" id="" href="<?php echo site_url('news/cover?id=0') ?>">Portada</a>
		<?php
		foreach ($categories as $category) { ?>

		<a class="btn btn-secondary  m-1" id=""
			href="<?php echo site_url('news/cover?id=' . $category->id) ?>"><?php echo $category->nombre; ?></a>
		<?php } ?>

	</div>
	<div class="card-group" id="tags">
		<?php
		foreach ($tags as $id => $tag) { ?>
		<!-- <a class="btn btn-secondary btn-sm" id="button-login" href="newsCover.php?id=0">Portada</a> -->
		<a class="btn btn-secondary btn-sm m-1 button-login" id="<?php echo $id ?>"
			href=""><?php echo $tag->name; ?></a>
		<?php } ?>

	</div>

	<div class='d-flex flex-row align-items-center mt-4'>
		<input class="form-control   mr-3 " type="search" placeholder="Search" aria-label="Search" id="text">
		<button class="btn btn-secondary " type="submit">Search</button>
	</div>
</div>





<div class="container ">
	<main class="bg-white  ">
		<div class="row" id='datos'>
			<?php
			if (isset($news)) {
				foreach ($news as $new) { ?>


			<div class="col mb-4 col-md-6 col-lg-4">
				<div class="card">
					<div class="card-body">
						<p class="card-text"><?php
														echo  $new->fecha ?></p>
						<h5 class="card-title"><?php echo $new->title; ?></h5>
						<p class="card-text"><?php echo $new->short_description; ?></p>
						<a class="btn btn-secondary btn-sm " id="button-login"
							href="<?php echo $new->permanlink; ?>">See
							News</a>
					</div>
				</div>
			</div>

			<?php }
			} else {

				echo 'There is not data';
			} ?>


		</div>
	</main>
</div>



<div class="container pt-5 ">
	<footer class="bg-white  pt-2">
		<ul class="nav justify-content-center ">
			<li class="nav-item active ">
				<a class="nav-link text-secondary " href="">My Cover </a>
			</li>
			<li class="nav-item ">
				<a class="nav-link text-secondary ">|</a>
			</li>
			<li class="nav-item ">
				<a class="nav-link text-secondary " href="">About</a>
			</li>
			<li class="nav-item ">
				<a class="nav-link text-secondary ">|</a>
			</li>
			<li class="nav-item ">
				<a class="nav-link text-secondary " href="# ">Help</a>
			</li>
		</ul>
		<ul class="nav justify-content-center ">
			<a class="nav-link text-secondary " href="# " tabindex="-1 " aria-disabled="true ">© My News Cover
			</a>
		</ul>
	</footer>
</div>
</body>


<script src="https://code.jquery.com/jquery-3.5.1.min.js"
	integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
	integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
</script>

<script src="<?php echo base_url('assets/ajax.js'); ?>"></script>

</html>
