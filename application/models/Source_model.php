<?php




class Source_model extends CI_Model
{
    /**
     * Undocumented function
     *this method insert a source
     * @param [type] $data source data
     * @return void if the source is insert
     */
    public function insert($data)
    {
        $this->db->insert('source', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Undocumented function
     *return a list by user id
     * @param [type] $id user id
     * @return void the list
     */
    public function sources($id)
    {

        $this->db->where('id_user', $id);
        $query = $this->db->get('source');
        return $query->result();
    }

    /**
     * Undocumented function
     *return a list of sources
     * @return void the list
     */
    public function allSources()
    {

        $query = $this->db->get('source');
        return $query->result();
    }

    /**
     * Undocumented function
     *Return a list of sources by id user
     * @param [type] $id user id
     * @return voidthe list
     */
    public function all($id)
    {


        $this->db->select('s.id, url, name, id_user, id_category, private, nombre ');
        $this->db->from('source s');
        $this->db->join('category c', 'c.id = s.id_category ');
        $this->db->where('id_user', $id);
        $query = $this->db->get();
        return $query->result();
    }



    /**
     * Undocumented function
     *This methos edit a source
     * @param [type] $id source id
     * @param [type] $data source id
     * @return void if the source is edited
     */
    public function editSource($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('source', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Undocumented function
     *This method return a source by id
     * @param [type] $id source id
     * @return void the source
     */
    public function sourcebyid($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('source');
        return $query->row();
    }



    /**
     * Undocumented function
     *delete a source by id of category
     * @param [type] $id category id
     * @return void is source is deleted
     */
    public function delete($data)
    {
        $this->db->delete('source', $data);
    }

    /**
     * Undocumented function
     *delete a source by id of category
     * @param [type] $id category id
     * @return void is source is deleted
     */
    public function deleteByCategory($id)
    {

        $this->db->where('id_category', $id);

        $this->db->delete('source');
    }
}
