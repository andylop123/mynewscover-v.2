<?php




class Category_model extends CI_Model
{

    /**
     * Undocumented function
     *return a list of categories
     * @return void the list
     */
    public function all()
    {
        $query = $this->db->get('category');
        return $query->result();
    }

    /**
     * Undocumented function
     *return a list of categories by news
     * @return void the list
     */
    public function categoryNews()
    {
        $this->db->distinct();
        $this->db->select('c.id, c.nombre');
        $this->db->from('category c');
        $this->db->join('news n', 'c.id = n.id_category');
        $query = $this->db->get();
        return $query->result();
    }



    /**
     * Undocumented function
     *this method removes a category by id 
     * @param [type] $data category data
     */
    public function delete($data)
    {
        $this->db->delete('category', $data);
    }



    /**
     * Undocumented function
     *return a category by id
     * @param [type] $id id category
     * @return void the category
     */
    public function categoryById($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('category');
        return $query->row();
    }

    /**
     * Undocumented function
     *this method insert a category
     * @param [type] $data category data
     * @return void if the category is insert
     */
    public function insert($data)
    {
        $this->db->insert('category', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Undocumented function
     *this method edit a category
     * @param [type] $data category data
     * @return void if the category was inserted
     */
    public function editCategory($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('category', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
