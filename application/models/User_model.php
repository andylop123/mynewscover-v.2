<?php




class User_model extends CI_Model
{

    /**
     * Undocumented function
     *this method checks if a user exists
     * @param [type] $username user email
     * @param [type] $password user password
     * @return object if a user exist
     */
    public function authenticate($username, $password)
    {

        $this->db->where('email', $username);
        $this->db->where('password', $password);
        $query = $this->db->get('users');
        return $query->row();
    }


    /**
     * Undocumented function
     *this method checks if a user exists
     * @param [type] $name user name
     * @param [type] $lastName user lastname
     * @return object if a user exist
     */
    public function userToPublic($name, $lastName)
    {

        $this->db->where('name', $name);
        $this->db->where('lastname', $lastName);
        $this->db->where('id_rol', 1);
        $query = $this->db->get('users');
        return $query->row();
    }




    /**
     * Undocumented function
     * this method return a list
     * @return void The list
     */
    public function all()
    {
        $query = $this->db->get('usuario');
        return $query->result();
    }

    /**
     * Undocumented function
     * this method return a list
     * @return void The list
     */
    public function rol()
    {
        $query = $this->db->get('rol');
        return $query->result();
    }



    /**
     * Undocumented function
     *This method delete a user
     * @param [type] $data user id
     * @return void if the user is deleted
     */
    public function delete($data)
    {
        $this->db->delete('usuario', $data);
    }

    /**
     * Undocumented function
     * this method return the last user
     * @return void The last user
     */
    public function lastUser()
    {

        $this->db->limit(1);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('users');
        return $query->row();
    }

    /**
     * Undocumented function
     *this method return a user by id
     * @param [type] $id user id
     * @return void if the user exist
     */
    public function userById($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('usuario');
        return $query->row();
    }

    /**
     * Undocumented function
     *This method insert a user
     * @param [type] $data user sata
     * @return void if the user is inserted
     */
    public function insert($data)
    {
        $this->db->insert('users', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * Undocumented function
     *this method edit a user
     * @param [type] $id user id
     * @param [type] $data user data
     * @return void if the user is edited
     */
    public function editUser($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('users', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
