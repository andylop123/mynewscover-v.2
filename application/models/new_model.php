<?php




class new_model extends CI_Model
{
    /**
     * Undocumented function
     *insert a news
     * @param [type] $data news data
     * @return void if news was inserted
     */
    public function insert($data)
    {
        $this->db->insert('news', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Undocumented function
     *Drop the data of the table 
     */
    public function dropTable()
    {
        $this->db->empty_table('news');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Undocumented function
     *removes a news by id category or id sources id
     * @param [type] $id category or source id;
     * @return void if it were deleted
     */
    public function deleteNews($id, $bool)
    {
        if ($bool) {
            $this->db->where('id_category', $id);
        } else {
            $this->db->where('id_source', $id);
        }
        $this->db->delete('news');
    }

    /**
     * Undocumented function
     *return all the news or news by categry id
     * @param [type] $id categry id
     * @param [type] $id_user user id
     * @return void the list
     */
    public function news($id, $id_user)
    {
        if ($id != 0) {
            $this->db->where('id_category', $id);
        }
        $this->db->where('id_user', $id_user);
        $query = $this->db->get('news');
        $this->db->limit(1, 30);
        return $query->result();
    }

    /**
     * Undocumented function
     *return all the news or news by categry id and if it is public
     * @param [type] $id categry id
     * @param [type] $id_user user id
     * @return void the list
     */
    public function newsPublic($id, $id_user)
    {
        if ($id != 0) {
            $this->db->where('id_category', $id);
        }
        $this->db->where('id_user', $id_user);
        $this->db->where('public', True);
        $query = $this->db->get('news');
        $this->db->limit(1, 30);
        return $query->result();
    }



    /**
     * Undocumented function
     *return the list of news by name of the new
     * @param [type] $name news name
     * @param [type] $id_user user id
     * @return void the list
     */
    public function newsByNombre($name, $id_user)
    {

        $this->db->like('title', $name);
        $this->db->or_like('short_description', $name);
        $this->db->where('id_user', $id_user);
        $query = $this->db->get('news');
        return $query->result();
    }

    /**
     * Undocumented function
     *return the list of news by name of the news if is public
     * @param [type] $name news name
     * @param [type] $id_user user id
     * @return void the list
     */
    public function newsByNombrePublic($name, $id_user)
    {

        $this->db->like('title', $name);
        $this->db->or_like('short_description', $name);
        $this->db->where('id_user', $id_user);
        $this->db->where('public', True);
        $query = $this->db->get('news');
        return $query->result();
    }

    /**
     * Undocumented function
     *return the list of news by name of the tag
     * @param [type] $name tag name
     * @param [type] $id_user user id
     * @return void the list
     */
    public function newsByTag($name, $id_user)
    {

        $this->db->where('id_user', $id_user);
        $this->db->where('name_tag', $name);
        $query = $this->db->get('news');
        return $query->result();
    }


    /**
     * Undocumented function
     *return the list of news by name of the tag and if is public
     * @param [type] $name tag name
     * @param [type] $id_user user id
     * @return void the list
     */
    public function newsByTagPublic($name, $id_user)
    {

        $this->db->where('id_user', $id_user);
        $this->db->where('name_tag', $name);
        $this->db->where('public', True);
        $query = $this->db->get('news');
        return $query->result();
    }

    /**
     * Undocumented function
     *return a list of news if public is true
     * @return void the list
     */
    public function publics()
    {
        // select DISTINCT u.id, u.name,u.lastname from users u INNER join news n on u.id = n.id_user where n.public = 'true'
        $this->db->distinct();
        $this->db->select('u.id, u.name,u.lastname');
        $this->db->from('users u');
        $this->db->join('news n', ' u.id = n.id_user');
        $this->db->where('n.public', True);
        $query = $this->db->get();
        return $query->result();
    }

    /**
     * Undocumented function
     *This method makes the news public
     * @param [type] $id user id
     * @param [type] $data news data
     * @return void if the news were publics
     */
    public function publish($id, $data)
    {
        $this->db->where('id_user', $id);
        $this->db->update('news', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
