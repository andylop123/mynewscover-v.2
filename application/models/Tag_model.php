<?php




class Tag_model extends CI_Model
{

    /**
     * Undocumented function
     *This method return a list of tags+
     * @return void the lists of tags
     */
    public function all()
    {
        $query = $this->db->get('tags');
        return $query->result();
    }
    /**
     * Undocumented function
     *This method return a list of tags by id
     * @param [type] $id category id
     * @return void the list with this id in category
     */
    public function tags($id)
    {
        $this->db->where('id_category', $id);
        $query = $this->db->get('tags');
        return $query->result();
    }


    /**
     * Undocumented function
     *this method drop de table 
     * @return void if the table is dropped
     */
    public function dropTable()
    {
        $this->db->empty_table('tags');
        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Undocumented function
     *return the tag with that name
     * @param [type] $name tags name
     * @return void the tags
     */
    public function tag($name)
    {
        $this->db->where('name', $name);
        $query = $this->db->get('tags');
        return $query->row();
    }




    /**
     * Undocumented function
     *This method insert a tag
     * @param [type] $data tag sata
     * @return void if the tag is inserted
     */
    public function insert($data)
    {
        $this->db->insert('tags', $data);

        if ($this->db->affected_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * delete a tag by id of category
     */   public function deleteByCategory($id)
    {

        $this->db->where('id_category', $id);

        $this->db->delete('tags');
    }
}
