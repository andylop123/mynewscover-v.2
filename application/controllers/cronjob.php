<?php
defined('BASEPATH') or exit('No direct script access allowed');

class cronjob extends CI_Controller
{
    /**
     * Undocumented function
     *insert the news by rss feed
     */
    public function insert()
    {
        $this->load->model('new_model');
        $this->load->model('Tag_model');
        $this->Tag_model->dropTable();
        $this->new_model->dropTable();
        $this->load->model('Source_model');
        $sources = $this->Source_model->allSources();

        foreach ($sources as $source) {
            $url = $source->url;
            $rss = simplexml_load_file($url);
            foreach ($rss->channel->item as $item) {
                // echo $this->convert($item->pubDate);
                $this->insertTag($source, $item);

                $description = $item->description;  //extrae la descripcion
                $item->title = str_replace("'", " ", $item->title);
                $item->title = str_replace('"', " ", $item->title);
                $item->pubDate = $this->convert($item->pubDate);
                if (strlen($description) > 400) { //limita la descripcion a 400 caracteres
                    $stringCut = substr($description, 0, 200);
                    $item->description = substr($stringCut, 0, strrpos($stringCut, ' ')) . '...';
                    $this->insertNew($source, $item);
                }
            }
        }
    }

    /**
     * Undocumented function
     *insert a tag
     * @param [type] $source source data
     * @param [type] $newTag rss category
     */
    public function insertTag($source, $newTag)
    {
        $this->load->model('Tag_model');
        $tags = $this->Tag_model->all();
        $data = array(
            'name' => $newTag->category,
            'id_user' => $source->id_user,
            'id_category' => $source->id_category
        );

        $countTags = count($tags);
        if ($countTags == 0) {
            $this->Tag_model->insert($data);
        }
        $tag = $this->Tag_model->tag($newTag->category);
        $repite = False;
        if (!$tag) {
            $this->Tag_model->insert($data);
        }
    }

    /**
     * Undocumented function
     *insert a new
     * @param [type] $source sources data
     * @param [type] $item news data
     */
    public function insertNew($source, $item)
    {

        $this->load->model('new_model');
        $data = array(
            'title' => $item->title,
            'short_description' => $item->description,
            'permanlink' => $item->link,
            'fecha' => $item->pubDate,
            'id_source' => $source->id,
            'id_user' => $source->id_user,
            'id_category' => $source->id_category,
            'name_tag' => $item->category,
        );
        $result = $this->new_model->insert($data);
        if ($result) {
            echo 'inserto';
        } else {
            echo 'no inserto';
        }
    }


    /**
     * Undocumented function
     *this methos convert a date in other shape
     * @param [type] $fecha date to convert
     * @return void date convert
     */
    public function convert($fecha)
    {
        date_default_timezone_set("America/Costa_Rica");
        $dt = new DateTime($fecha);
        return $dt->format('d/m/Y   H:i A');
    }
}
