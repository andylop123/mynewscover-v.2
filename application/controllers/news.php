<?php
defined('BASEPATH') or exit('No direct script access allowed');

class news extends CI_Controller
{

	/**
	 * Undocumented function
	 *show cover private or public
	 */
	public function cover()
	{


		$user = $this->session->userdata('user');
		$this->load->model('Source_model');
		$this->load->model('Category_model');
		$this->load->model('new_model');
		$this->load->model('Tag_model');
		$id = $this->input->get('id');

		if ($user) {
			$sources = count($this->Source_model->sources($user->id));
			if ($sources > 0) {
				// $data['sources'] = $this->Source_model->all();
				$data['categories'] = $this->Category_model->categoryNews();
				$data['tags'] = $this->Tag_model->tags($id);
				$data['news'] = $this->new_model->news($id, $user->id);
				$data['user'] = $user;
				$this->load->view('news', $data);
			} else {
				redirect('source/showsource');
			}
		} else {
			redirect('user/login');
		}
	}


	/**
	 * Undocumented function
	 *public my cover
	 */
	function publicCover()
	{
		$user = $this->session->userdata('user');
		if ($user) {
			$data['user'] = $user;
			$this->load->view('covers', $data);
		} else {
			redirect('user/login');
		}
	}

	/**
	 * Undocumented function
	 *return a lists the news by nombre
	 * @return void news by nombre
	 */
	function newsByNombre()
	{
		$user = $this->session->userdata('user');
		if ($user) {
			$this->load->model('new_model');
			$text = $this->input->post('texto');
			$resultado = $this->new_model->newsByNombre($text, $user->id);
			echo json_encode($resultado);
		}
	}

	/**
	 * Undocumented function
	 *return a lists the news by nombre
	 * @return void news by nombre
	 */
	function newsByNombrePublic()
	{
		$user = $this->session->userdata('user');
		if ($user) {
			$this->load->model('new_model');
			$text = $this->input->post('texto');
			$resultado = $this->new_model->newsByNombrePublic($text, $user->id);
			echo json_encode($resultado);
		}
	}
	/**
	 * Undocumented function
	 *return a lists the news by tag
	 * @return void news by tag
	 */
	function newsByTag()
	{
		$user = $this->session->userdata('user');
		if ($user) {
			$this->load->model('new_model');
			$tags = json_decode($this->input->post('tags'));
			foreach ($tags as $tag) {

				$resultado = $this->new_model->newsByTag($tag, $user->id);
				echo json_encode($resultado);
			}
		}
	}
	/**
	 * Undocumented function
	 *return a lists the news by tag
	 * @return void news by tag
	 */
	function newsByTagPublic()
	{
		$user = $this->session->userdata('user');
		if ($user) {
			$this->load->model('new_model');
			$tags = json_decode($this->input->post('tags'));
			foreach ($tags as $tag) {

				$resultado = $this->new_model->newsByTagPublic($tag, $user->id);
				echo json_encode($resultado);
			}
		}
	}





	/**
	 * Undocumented function
	 *public the cover
	 */
	function publish()
	{
		$user = $this->session->userdata('user');
		$this->load->model('new_model');
		if ($user) {
			$data = array(
				'public' => true
			);
			$result = $this->new_model->publish($user->id, $data);

			if ($result) {
				redirect('news/cover');
			} else {
				redirect('news/cover');
			}
		} else {
			redirect('user/login');
		}
	}



	/**
	 * Undocumented function
	 * show links to cover public
	 */
	public function publicCovers()
	{

		$this->load->model('new_model');
		$data['links'] = $this->new_model->publics();
		$this->load->view('publiccovers', $data);
	}


	/**
	 * Undocumented function
	 * show cover4 public by user
	 * @param [type] $name user name
	 * @param [type] $lastName user lastname
	 */
	public function pCover($name, $lastName)
	{

		$this->load->model('User_model');
		$user = $this->User_model->userToPublic($name, $lastName);
		$this->session->set_userdata('user', $user);
		redirect('news/newsPublic');
	}

	/**
	 * Undocumented function
	 * show cover public
	 */
	public function newsPublic()
	{
		$user = $this->session->userdata('user');
		$this->load->model('Category_model');
		$this->load->model('new_model');
		$this->load->model('Tag_model');
		$id = $this->input->get('id');


		// 		// $data['sources'] = $this->Source_model->all();
		$data['categories'] = $this->Category_model->categoryNews();
		$data['tags'] = $this->Tag_model->tags($id);
		$data['news'] = $this->new_model->newsPublic($id, $user->id);
		$data['user'] = $user;
		$this->load->view('PublicNews', $data);
	}
}
