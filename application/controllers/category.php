<?php
defined('BASEPATH') or exit('No direct script access allowed');

class category extends CI_Controller
{



    /**
     * Undocumented function
     * show de categories
     */
    public function index()
    {


        $user = $this->session->userdata('user');
        if ($user) {
            $this->load->model('Category_model');
            $data['categories'] =  $this->Category_model->all();
            $this->load->view('categories', $data);
        } else {

            redirect('user/login');
        }
    }

    /**
     * Undocumented function
     *Show add category
     */
    public function ShowAddCategory()
    {
        $user = $this->session->userdata('user');
        if ($user) {
            $this->load->view('addnewcategory');
        } else {

            redirect('user/login');
        }
    }

    /**
     * Undocumented function
     *insert the category
     */
    public function addCategory()
    {


        $user = $this->session->userdata('user');
        if ($user) {
            $this->load->model('Category_model');
            $data = $this->input->post();
            $result = $this->Category_model->insert($data);

            if ($result) {
                redirect('category/index');

                // $email->sendEmail($user);
            } else {

                // send errors
                $this->session->set_flashdata('msg', 'There was an error');
                redirect('category/showaddcategory');
            }
        } else {

            redirect('user/login');
        }
    }
    /**
     * Undocumented function
     *delete the category 
     */
    public function delete()
    {
        $this->load->model('Category_model');
        $this->load->model('new_model');
        $this->load->model('Source_model');
        $this->load->model('Tag_model');
        $data = $this->input->get();

        $this->new_model->deleteNews($data['id'], true);
        $this->Tag_model->deleteByCategory($data['id']);
        $this->Source_model->deleteByCategory($data['id']);
        $this->Category_model->delete($data);
        redirect('category/index');;
    }

    /**
     * Undocumented function
     *show edit category
     */
    public function showCategory()
    {

        $user = $this->session->userdata('user');

        if ($user) {
            $id = $this->input->get('id');
            $this->load->model('Category_model');
            $data['category'] = $this->Category_model->categoryById($id);
            $data['user'] = $user;

            $this->load->view('editcategory', $data);
        } else {
            redirect('user/login');
            // redirect('category/index');
        }
    }

    /**
     * Undocumented function
     *edit the category
     */
    public function edit()
    {

        $this->load->model('Category_model');
        $data = $this->input->post();
        $id = $this->input->post('id');

        echo $id;
        // print_r($data);

        $result = $this->Category_model->editCategory($id, $data);

        if ($result) {
            redirect('category/index');

            //     // $email->sendEmail($user);
        } else {

            //     // send errors
            $this->session->set_flashdata('msg', 'There was an error');
            redirect('category/showcategory?id=' . $id);
        }
    }
}
