<?php
defined('BASEPATH') or exit('No direct script access allowed');

class user extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/user
	 *	- or -
	 * 		http://example.com/index.php/user/login
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function login()
	{
		$this->load->view('login');
	}


	/**
	 * Undocumented function
	 *anthenticate a user
	 * @return void if the user exit
	 */
	public function authenticate()
	{

		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$this->load->model('User_model');
		$user = $this->User_model->authenticate($email, $password);

		$this->load->model('Source_model');

		if ($user) {
			$sources = count($this->Source_model->sources($user->id));
			$this->session->set_userdata('user', $user);
			if ($user->verify == '1') {
				if ($user->id_rol == 2) {
					echo 'Administrador';
					redirect('category/index');
				} else {
					if ($sources < 0) {
						redirect('source/showsource');
					} else {

						redirect('news/cover');
					}
				}
			} else {
				$this->session->set_flashdata('msg', 'User must be verified');
				redirect('user/login');
			}
		} else {
			$this->session->set_flashdata('msg', 'There was an error');
			redirect('user/login');
		}
	}


	/**
	 * Undocumented function
	 *verify the user mail
	 */
	public function checkmail()
	{

		$this->load->model('User_model');
		$id = $this->input->get('id');
		$data = array(
			'verify' => true
		);
		$result = $this->User_model->editUser($id, $data);

		if ($result) {
			$this->load->view('checkmail');
		}
	}

	/**
	 * Undocumented function
	 *show the singup to user
	 */
	public function singup()
	{
		$this->load->model('User_model');
		$data['rols'] = $this->User_model->rol();

		$this->load->view('singup', $data);
	}

	/**
	 * Undocumented function
	 *send mail to user
	 * @param [type] $user user to send mail
	 */
	public function sendEmail($user)
	{
		$this->load->library('email');

		$this->email->from("mynewscoverv2@gmail.com", $user->name);
		$this->email->to("mynewscoverv2@gmail.com");

		$this->email->subject('Verify Mail');
		$this->email->message('Verify your email by clicking on the following link: ' . site_url('user/checkmail?id=' . $user->id));
		if ($this->email->send()) {

			redirect('user/login');
		}
	}

	/**
	 * Undocumented function
	 *this method insert a user
	 */
	public function insert()
	{

		$this->load->model('User_model');
		$data = $this->input->post();
		$result = $this->User_model->insert($data);
		$user = $this->User_model->lastUser();


		if ($result) {
			$this->session->set_flashdata('msg', 'Check your mail to activate your account.');
			$this->sendEmail($user);
			// $email->sendEmail($user);
		} else {

			// send errors
			$this->session->set_flashdata('msg', 'There was an error');
			redirect('user/singup');
		}
	}

	/**
	 * Undocumented function
	 *close the session
	 */
	public function logout()
	{

		$this->session->unset_userdata('user');
		redirect('user/login');
	}
}
