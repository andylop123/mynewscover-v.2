<?php
defined('BASEPATH') or exit('No direct script access allowed');

class source extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		$this->load->library('session');
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/source
	 *	- or -
	 * 		http://example.com/index.php/source/showSource
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function showSource()
	{

		$user = $this->session->userdata('user');

		if ($user) {
			$this->load->model('Source_model');
			$data['sources'] = $this->Source_model->all($user->id);
			$data['sourcesId'] = $this->Source_model->sources($user->id);
			$data['user'] = $user;
			$this->load->view('sources', $data);
		} else {
			redirect('user/login');
		}
	}

	/**
	 * Undocumented function
	 * show add source
	 */
	public function showaddSource()
	{

		$user = $this->session->userdata('user');

		if ($user) {
			$data['user'] = $user;
			$this->load->model('Category_model');
			$data['categories'] = $this->Category_model->all();
			$this->load->view('addsource', $data);
		} else {
			redirect('user/login');
		}
	}


	/**
	 * Undocumented function
	 *insert source
	 */
	public function insert()
	{
		$this->load->model('Source_model');
		$data = $this->input->post();
		$result = $this->Source_model->insert($data);


		if ($result) {
			redirect('source/showsource');
			// $email->sendEmail($user);
		} else {

			// send errors
			$this->session->set_flashdata('msg', 'There was an error');
			redirect('source/showaddsource');
		}
	}


	/**
	 * Undocumented function
	 * Show edit source
	 */
	public function showEdit()
	{




		$user = $this->session->userdata('user');

		if ($user) {
			$this->load->model('Source_model');
			$id = $this->input->get('id');
			$data['source'] = $this->Source_model->sourcebyid($id);

			$this->load->model('Category_model');
			$data['categories'] = $this->Category_model->all();

			$data['user'] = $user;
			$this->load->view('editsource', $data);
		} else {
			redirect('user/login');
		}
	}


	/**
	 * Undocumented function
	 *edit source
	 */
	public function edit()
	{
		$this->load->model('Source_model');
		$data = $this->input->post();
		// $id = $this->input->post('id_source');
		$id = $this->input->get('id');

		// echo $id;
		// print_r($data);

		$result = $this->Source_model->editSource($id, $data);

		if ($result) {
			redirect('source/showsource');

			// 	//     // $email->sendEmail($user);
		} else {

			// 	//     // send errors
			$this->session->set_flashdata('msg', 'There was an error');
			redirect('source/showedit?id=' . $id);
		}
	}

	/**
	 * Undocumented function
	 *delete a source
	 */
	public function delete()
	{
		$this->load->model('Source_model');
		$this->load->model('new_model');
		$data = $this->input->get();
		$this->new_model->deleteNews($data['id'], false);
		$this->Source_model->delete($data);
		redirect('source/showsource');
	}
}
